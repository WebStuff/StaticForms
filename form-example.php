<?php $formpg = basename(__FILE__); include_once('form.inc.php'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Form Example</title>
</head>

<body>
<?php
if (!empty($error_msg)) {
    echo '<div id="msg" class="alert alert-danger" role="alert"><strong>ERROR:</strong> '. implode(", ", $error_msg) . "</div>";
}
if ($result != NULL) {
    echo '<div id="msg" class="alert alert-success">'. $result . "</div>";
}
if ($_GET['result'] == 'success') {
    echo '<div id="msg" class="alert alert-success">'. $success . "</div>";
    $disable = true;
}
?>
<?php if (!isset($disable) && $disable !== true): ?>
        <form role="form" id="static_form" action="<?php echo basename(__FILE__); ?>" method="post"<?php if (strlen($uploadFields) > 1) echo 'enctype="multipart/form-data"';?>>
  <noscript>
    <p><input type="hidden" name="nojs" id="nojs" /></p>
  </noscript>
  
  <div class="form-group">
    <label for="exampleFormControlInput1">Name</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Name" name="name">
  </div>
  <div class="form-group">
    <label for="exampleFormControlInput2">Email address</label>
    <input type="email" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com" name="email">
  </div>
    <div class="form-group">
    <label for="exampleFormControlTextarea1">Comments</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="comments"></textarea>
  </div>

    <div class="form-group hide">
        <label for="InputReal">What is 4+3? (Bot Test)</label>
        <div class="input-group">
          <input type="text" class="form-control" name="InputReal" id="InputReal" value="<?php echo $_POST['InputReal'];?>">
          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
      </div>

      <button type="submit" class="btn btn-info pull-right g-recaptcha" data-sitekey="<?php echo $reCaptcha_sitekey;?>" data-callback="submitForm" data-action="submit"<?php if (isset($disable) && $disable === true) echo ' disabled="disabled"'; ?>>Submit Application</button>

  </form>
  <?php 
  if( $reCaptcha): ?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
  function submitForm(token) {
      document.getElementById('static_form').submit();
  }
  </script>
  <?php 
  endif;
endif; ?>

</body>
</html>