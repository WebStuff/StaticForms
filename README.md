# imageDESIGN Static Forms
#### v1.6 _February 2, 2018_

Paste these files in this directory into the same directory as your form page - form.inc.php & email-template.html & uploaded & sentmail directories

1. on the page with the form, change the extension from .htm or .html to .php

2. at the top of the page, put this before <!DOCTYPE html>:
```
	<?php $formpg = basename(\_\_FILE\_\_); include_once('form.inc.php'); ?>
```

3. Replace your opening `<form>` tag with the following:
```
	<?php
	if (!empty($error_msg)) {
		echo '<div class="alert alert-danger" role="alert"><strong>ERROR:</strong> '. implode(", ", $error_msg) . "</div>";
	}
	if ($result != NULL) {
		echo '<div class="alert alert-success">'. $result . "</div>";
	}
	if ($_GET['result'] == 'success') {
		echo '<div class="alert alert-success">'. $success . "</div>";
		$disable = true;
	}
	?>
	<?php if (!isset($disable) && $disable !== true): ?>
			<form role="form" action="<?php echo basename(__FILE__); ?>" method="post"<?php if (strlen($uploadFields) > 1) echo 'enctype="multipart/form-data"';?>>
	  <noscript>
		<p><input type="hidden" name="nojs" id="nojs" /></p>
	  </noscript>
```
4. Replace the end of the `<form>` tag and any Submit buttons with the following:
```
		<div class="form-group d-none">
			<label for="InputReal">What is 4+3? (Bot Test)</label>
			<div class="input-group">
			  <input type="text" class="form-control" name="InputReal" id="InputReal" value="<?php echo $_POST['InputReal'];?>">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
		  </div>
		  
		  <button type="submit" class="btn btn-info pull-right"<?php if (isset($disable) && $disable === true) echo ' disabled="disabled"'; ?>>Submit</button>
		  
		
	  </form>
	  <?php endif; ?>
```	  
5. Edit the settings at the top of form.inc.php

6. You can modify email-template.html css if you like as well but it will work without any modifications.