<?php
/* INCLUDE THIS FILE ABOVE YOUR <html> TAG AND BE SURE TO SAVE YOUR PAGE AS .php INSTEAD of .html

imageDESIGN Static Forms - Now with POSTMARKAPP & HEADING support!
v1.71 | May 26, 2020

/****** HEADINGS CAN BE ADDED WITHIN YOUR RESULTS USING <input type="hidden" name="HEADING_{desc}" value="Title"> ************/

// OPTIONS - PLEASE CONFIGURE THESE BEFORE USE!
$testmode = 1; //SET TO 1 to display email instead of sending, SET TO 0 for normal operation
$toEmail = ''; // the email address you wish to receive these mails through
$Cc = ''; //Carbon copy to this address
$Bcc = 'admin@imagedesign.pro'; //Blind Carbon copy to this address

$sendtoCustomer = 0; //set to 1 to send email to customer as well
$customerEmail = $_POST['email']; //Customer email

$useSMTP = 0; //Change to 1 to enable Postmark App SMTP
$smtpHost = 'smtp.postmarkapp.com';
$smtpUser = '4ad1b144-f1c5-4cf7-80e2-7ba3ba198c44';
$smtpPass = '4ad1b144-f1c5-4cf7-80e2-7ba3ba198c44';
$smtpAuth = 'tls';
$smtpPort = 587;

$reCaptcha = 0; //Set to 1 to enable Google reCAPTCHA v3
$reCaptcha_sitekey = ''; //GET v3 keys FROM https://www.google.com/recaptcha/admin/
$reCaptcha_secretkey = '';

$yourWebsite = "WEBSITE NAME"; // the name of your website
$domain = $_SERVER['HTTP_HOST']; //domain of website - no http:// or https:// update manually if not working
$yourLogo = 'https://'.$domain.'/img/your-logo.png'; //logo location that appears at top of email
$replyTo = ''; //Who should customers reply to if send to customer enabled

$maxPoints = 4; // max points a person can hit before it refuses to submit - recommend 4
$requiredFields = "name,email,comments"; // names of the fields you'd like to be required as a minimum, separate each field with a comma
$hiddenFields = "g-recaptcha-response,InputReal,submit"; //names of fields you don't need to show on the email
$emailFields = "email";// names of any required email fields, separate each field with a comma (should also be included in $requiredFields)
$urlFields = ""; // names of any required URL/website fields, separate each field with a comma (should also be included in $requiredFields)
$uploadFields = ""; //If you have any fields for uploading, enter the names here separated with commas.
$uploadDir = "uploaded/"; // Upload Directory
$maxUploadSize = 5; // max size in MB
$saveEmails = 0; //Set to 1 to save a copy of all emails to the sent directory, set to 0 to disable
$sentDir = "sentmail/"; //Save sent emails Directory
$saveName = ""; //file name prefix for saved emails
$allowedFiles = ''; //Allowed file extensions comma separated
$anchor = "#msg"; //Have your form jump to an anchor point ID on success -ie "#MyForm"
$templateFile = "email-template.html";
$returnPage = "/"; //Return back to this page
$hideNumPrefix = true; //if any fields have number prefixes (2_label, 3_label) set them to show or hide (label) here
$heading = "Contact Form Submission"; //Main Email heading
$lead = "The following submission was sent from <a href='$formpg'>https://$domain/$formpg</a>"; //lead in text after heading
$subject = "New Contact Form Submission"; //Email Subject

$success = '<strong>Your submission was successfully sent.</strong>'; //Message shown to user on successful submission

// DO NOT EDIT BELOW HERE
include_once('phpmailer/PHPMailer.php');
include_once('phpmailer/SMTP.php');
include_once('phpmailer/Exception.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$regex = '/(?<!^)((?<![[:upper:]])[[:upper:]]|[[:upper:]](?![[:upper:]]))/';
$error_msg = array();
$result = null;
$formpg = $domain.'/'.$formpg;
$requiredFields = explode(",", $requiredFields);
$hiddenFields = explode(",", $hiddenFields);
$emailFields = explode(",", $emailFields);
$urlFields = explode(",", $urlFields);

function clean($data) {
	$data = trim(stripslashes(strip_tags($data)));
	return $data;
}
function isBot() {
	$bots = array("Indy", "Blaiz", "Java", "libwww-perl", "Python", "OutfoxBot", "User-Agent", "PycURL", "AlphaServer", "T8Abot", "Syntryx", "WinHttp", "WebBandit", "nicebot", "Teoma", "alexa", "froogle", "inktomi", "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory", "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot", "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz");

	foreach ($bots as $bot)
		if (stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
			return true;

	if (empty($_SERVER['HTTP_USER_AGENT']) || $_SERVER['HTTP_USER_AGENT'] == " ")
		return true;
	
	return false;
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {

	//Google ReCAPTCHA Validation
	if (isset($_POST['g-recaptcha-response']) && $reCaptcha) {
 
		//get verify response data
		$verifyCaptchaResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$reCaptcha_secretkey.'&response='.$_POST['g-recaptcha-response']);
		$responseCaptchaData = json_decode($verifyCaptchaResponse);
		if($responseCaptchaData->success) {
			//echo 'Captcha verified';
			//proceed with form values
		} else {
			$error_msg[] =  'Verification failed';
		}
	}

	if (isBot() !== false)
		$error_msg[] = "No bots please! UA reported as: ".$_SERVER['HTTP_USER_AGENT'];
		
	// lets check a few things - not enough to trigger an error on their own, but worth assigning a spam score.. 
	// score quickly adds up therefore allowing genuine users with 'accidental' score through but cutting out real spam :)
	$points = (int)0;
	
	$badwords = array("adult", "ass", "butt", "beastial", "bestial", "blowjob", "booty", "clit", "cum", "cunilingus", "cunillingus", "cunnilingus", "cunt", "ejaculate", "fag", "felatio", "fellatio", "fuck", "fuk", "fuks", "gay", "gangbang", "gangbanged", "gangbangs", "hotsex", "hardcode", "jism", "jiz", "lesbian", "orgasim", "orgasims", "orgasm", "orgasms", "phonesex", "phuk", "phuq", "pussies", "pussy", "porn", "sex", "sexy", "spunk", "teen", "xxx", "viagra", "phentermine", "tramadol", "adipex", "advai", "alprazolam", "ambien", "ambian", "amoxicillin", "antivert", "blackjack", "backgammon", "texas", "holdem", "poker", "carisoprodol", "ciara", "ciprofloxacin", "debt", "dating", "porn", "link=", "wanking", "voyeur", "content-type", "bcc:", "cc:", "document.cookie", "onclick", "onload", "javascript", "visitors", "promo", "traffic", "search", "Google", "YellowPages", "Yellow Pages", "reviews", "rating", "competitors", "clients","review");

	foreach ($badwords as $word)
		if (
			strpos(strtolower($_POST['info']), $word) !== false || 
			strpos(strtolower($_POST['first_name']), $word) !== false ||
			strpos(strtolower($_POST['last_name']), $word) !== false
		)
			$points += 2;
	
	if (strpos($_POST['request'], "http://") !== false || strpos($_POST['request'], "www.") !== false)
		$points += 2;
	if (isset($_POST['nojs']))
		$points += 1;
	if (preg_match("/(<.*>)/i", $_POST['request']))
		$points += 2;
	$spa = $_POST['InputReal'];
	if ( !empty($spa) && !($spa == "7" || $spa == "seven"))
		$points += 999;
	if (preg_match("/[bcdfghjklmnpqrstvwxyz]{7,}/i", $_POST['request']))
		$points += 1;
	// end score assignments

	//Uploads
	if( $uploadFields != false):
	$dir = $uploadDir;
	$filefields = explode(',',$uploadFields);
  
	foreach($filefields as $ff ):
		if( !$_FILES[$ff]['name'] && in_array($ff, $requiredFields) ){ 
			$error_msg[] = "Please select a file for <strong>".preg_replace($regex, ' $1', ucwords($ff))."</strong>";
			continue;
		}elseif( $_FILES[$ff]['name'] ){
			$filenm = str_replace(array(" ", '!', '@', '$', '%', '^', '^', '&', '*', '(', ')', '?', '>', '<', ',', ';', ':', '"', "'", '~', '`'), "_", basename($_FILES[$ff]['name']));
			$file = $dir . date('Ymdhis_') . $filenm;
			$upOK = 1;
			$filetype = pathinfo($file, PATHINFO_EXTENSION);
			//Check file size
			if ($_FILES[$ff]["size"] > ($maxUploadSize*1000000)) { //5MB
				$error_msg[] = "Sorry, your file is too large. Please upload a file no more than 5 MB";
				$upOK = 0;
			}
			// Allow certain file formats
			if( strpos($allowedFiles, $filetype) === false ) {
				$error_msg[] =  "Sorry, that file type is not allowed for <strong>".preg_replace($regex, ' $1', ucwords($ff))."</strong>.  You attached a <strong>.$filetype</strong> file, but we can only accept the following file types: <strong>.".str_replace(',', ', .',$allowedFiles).'</strong>';
				$upOK = 0;
			}
			if( $upOK ){
				if( move_uploaded_file($_FILES[$ff]['tmp_name'], $file) )	
					$filelink = 'http://'.$_SERVER['HTTP_HOST'].'/'.$file;
				else
					$error_msg[] =  "Sorry, there was an error uploading your file for <strong>".preg_replace($regex, ' $1', ucwords($ff))."</strong>.";	
			}
			$filecontent .=  '<a href="' .$filelink .'" target="_blank" title="Download ' . clean($filenm) . '" style="display:inline-block; padding: 5px 8px; border:solid 1px #FFF; background: #B9E5FF; color: #000; margin-right: 10px;">'.preg_replace($regex, ' $1', ucwords($ff)).' <span style="color:#FFF;">&#x25BC;</span></a>';
		}
	endforeach;
	endif; 
	
	foreach($requiredFields as $field) {
		trim($_POST[$field]);
		
		if (!isset($_POST[$field]) || empty($_POST[$field]) )
			$error_msg[] = "Please fill in the <strong>".preg_replace($regex, ' $1', ucwords($field))."</strong> field";
	}

	//if (!empty($_POST['name']) && !preg_match("/^[a-zA-Z-'\s]*$/", stripslashes($_POST['name'])))
		//$error_msg[] = "The name field must not contain special characters.\r\n";
	
	foreach( $emailFields as $ef ){
		if (!empty($_POST[$ef]) && !preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i', strtolower($_POST[$ef])))
		$error_msg[] = "Error in <strong>".preg_replace($regex, ' $1', ucwords($ef))."</strong> field - that is not a valid e-mail address.\r\n";
	}
	foreach( $urlFields as $uf ){
	if (!empty($_POST[$uf]) && !preg_match('/^(http|https):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i', $_POST[$uf]))
		$error_msg[] = "Error in <strong>".preg_replace($regex, ' $1', ucwords($uf))."</strong> field - Invalid website url.\r\n";
	}
	
	if ($error_msg == NULL && $points <= $maxPoints) {
		
		
		//$content = "You received this e-mail message through your website: \n\n";
		foreach ($_POST as $key => $val) {
      $label = str_replace(array('_', '-'), ' ', $key);
      if( $hideNumPrefix ){ $label = preg_replace('#^\d+#', '', $label);}
			if (is_array($val)) {
					$content .= '<tr><td valign="top" style="width:30%; padding-right:10px;"><strong>'. ucwords($label) . "</strong>:</td><td> ". implode(", ",$val). "</td><td class='expander'></td></tr>";
			} else if( !in_array($key, $hiddenFields) && strpos($key,'HEADING') === false) {
					$content .= '<tr><td valign="top" style="width:30%; padding-right:10px;"><strong>'. ucwords($label) . "</strong>:</td><td> " . nl2br(clean($val)) . "</td><td class='expander'></td></tr>";
			} else if (strpos($key,'HEADING') !== false){
        $content .= "<tr><th style='font-size: 25px; padding-top:15px; padding-bottom: 5px; border-bottom:dashed 2px #CCC;margin-bottom:5px;' colspan='3'>" . strtoupper(clean($val)) . "</th></tr>
        <tr><td col-span='3' style='height:5px'></td></tr>";
      }
		}

		$footer = '<p><strong>IP</strong>: '.$_SERVER['REMOTE_ADDR']."</p>";
		$footer .= '<p><strong>Browser</strong>: '.$_SERVER['HTTP_USER_AGENT']."</p>";
		$footer .= '<p><strong>Spam Points</strong>: '.$points.'</p>';
		$footer .= '<p><strong>Form Page</strong>: <a href="//'.$formpg.'" target="_blank">'.$formpg.'</a></p>';
		
		$message = file_get_contents($templateFile);
		$message = str_replace( array('[[HEADING]]', '[[LEAD]]', '[[CONTENT]]', '[[FILES]]', '[[FOOTER]]', '[[DOMAIN]]', '[[LOGO]]','[[COMPANY]]'), array( $heading, $lead, $content, $filecontent, $footer, $_SERVER['HTTP_HOST'].'/'.$formpg, $yourLogo, $yourWebsite), $message); 
		if($testmode == 1 ){ print_r($message); exit; }
		$headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
			$headers   .= "From: webform@{$_SERVER['HTTP_HOST']}\r\n";
		} else {
			$headers   .= "From: $yourWebsite <webform@{$_SERVER['HTTP_HOST']}>\r\n";	
		}
		$headers  .= "Reply-To: {$_POST['email_address']}\r\n";
    
    if( $CC )
      $headers .= "Cc: $CC\r\n";
    
    if( $BCC )
      $headers .= "Bcc: $BCC\r\n";
    
    
    $headers2  = 'MIME-Version: 1.0' . "\r\n";
    $headers2 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
			$headers2   .= "From: webform@{$_SERVER['HTTP_HOST']}\r\n";
		} else {
			$headers2   .= "From: $yourWebsite <webform@{$_SERVER['HTTP_HOST']}>\r\n";	
		}
		$headers2 .= "Reply-To: {$toEmail}\r\n";
    
    $recipients = array(
      'From'    =>  "webform@{$_SERVER['HTTP_HOST']}",
      'To'      =>  $toEmail,
      'ReplyTo' =>  $replyTo,
      'Cc'      =>  $Cc,
      'Bcc'     =>  $Bcc
    );
    $recipients2 = array(
      'From'    =>  "webform@{$_SERVER['HTTP_HOST']}",
      'To'      =>  $customerEmail,
      'ReplyTo' =>  $toEmail
    );
    $content = array(
      'HTML'    => true,
      'Subject' => $subject,
      'Body'    => $message
    );
    
		if (!$useSMTP && mail($toEmail,$subject,$message,$headers)) {
      if( $sendtoCustomer ){
        mail($customerEmail,$subject,$message,$headers2);
      }
      form_SaveEmail($message);
			if (!empty($thanksPage)) {
				header("Location: $thanksPage");
				exit;
			} else {
				$result = $success;
				$disable = true;
				header("Location: ".$returnPage."?result=success".$anchor);
				exit;
			}
    } else if( $useSMTP &&  sendSMTPemail($recipients, $content)){
      if( $sendtoCustomer ){
        //echo '#################################################Send to Customer';
         sendSMTPemail($recipients2, $content);
      }
      form_SaveEmail($message);
			if (!empty($thanksPage)) {
				header("Location: $thanksPage");
				exit;
			} else {
				$result = $success;
				$disable = true;
				header("Location: ".$returnPage."?result=success".$anchor);
				exit;
			}
      
		} else {
			$error_msg[] = '<strong>Your request could not be sent this time. ['.$points.']</strong>';
		}
	} else {
		if (empty($error_msg))
			$error_msg[] = '<strong>Your request looks too much like spam, and could not be sent this time. ['.$points.']</strong>';
	}
}

function sendSMTPemail($recipients, $content){
  global $smtpHost, $smtpUser, $smtpPass, $smtpAuth, $smtpPort;
  $mail = new PHPMailer(true);
  try{
    //Server settings
    $mail->SMTPDebug = 0; //2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $smtpHost;  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $smtpUser;                 // SMTP username
    $mail->Password = $smtpPass;                           // SMTP password
    $mail->SMTPSecure = $smtpAuth;                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $smtpPort;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom($recipients['From']);
    $mail->addAddress($recipients['To']);   
    if($recipients['ReplyTo'] ) $mail->addReplyTo($recipients['ReplyTo']);
    if($recipients['Cc'] ) $mail->addCC($recipients['Cc']);
    if($recipients['Bcc'] ) $mail->addBCC($recipients['Bcc']);

    //Attachments
   // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
   // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML($content['HTML']);                                  // Set email format to HTML
    $mail->Subject = $content['Subject'];
    $mail->Body    = $content['Body'];
   // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    return true;
  }catch(Exception $e){
    return 'Message could not be sent. Mailer Error: '. $mail->ERROR->Info;
  }
}

function form_SaveEmail($msg){
	global $sentDir, $saveEmails, $saveName;
	if( $saveEmails == 1 ):
		$filenm = $sentDir.date('Ymdhis_').$saveName.'.html';
		file_put_contents($filenm, $msg);
	endif;
}

function get_data($var) {
	if (isset($_POST[$var]))
		echo htmlspecialchars($_POST[$var]);
}